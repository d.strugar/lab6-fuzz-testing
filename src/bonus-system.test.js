import {calculateBonuses as calc} from './bonus-system'

const assert = require('assert')

const programs = {
  standard: {name: 'Standard', multiplier: 0.05},
  premium: {name: 'Premium', multiplier: 0.1},
  diamond: {name: 'Diamond', multiplier: 0.2},
  random: {name: 'Random', multiplier: 0},
}

const valuesRange = [
  [
    {amount: 9000, bonus: 1},
    {amount: 9999, bonus: 1},
    {amount: 0, bonus: 1},
    {amount: -1, bonus: 1},
    {amount: -5000, bonus: 1},
  ],
  [
    {amount: 10000, bonus: 1.5},
    {amount: 12500, bonus: 1.5},
    {amount: 14999, bonus: 1.5},
  ],
  [
    {amount: 50000, bonus: 2},
    {amount: 75000, bonus: 2},
    {amount: 99999, bonus: 2},
  ],
  [
    {amount: 100000, bonus: 2.5},
    {amount: 5000000, bonus: 2.5},
    {amount: Number.MAX_VALUE, bonus: 2.5},
  ],
]

describe('bonus tests', () => {
  let app
  console.log('bonus tests started')

  test('standard rate, amount < 10k', (done) => {
    const values = valuesRange[0]
    const {name, multiplier} = programs.standard
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier)
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier)

    done()
  })

  test('standard rate, 10k <= amount < 50k', (done) => {
    const values = valuesRange[1]
    const {name, multiplier} = programs.standard
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('standard rate, 50k <= amount < 100k', (done) => {
    const values = valuesRange[2]
    const {name, multiplier} = programs.standard
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('standard rate, 100k <= amount', (done) => {
    const values = valuesRange[3]
    const {name, multiplier} = programs.standard
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('premium rate, amount < 10k', (done) => {
    const values = valuesRange[0]
    const {name, multiplier} = programs.premium
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier)
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier)

    done()
  })

  test('premium rate, 10k <= amount < 50k', (done) => {
    const values = valuesRange[1]
    const {name, multiplier} = programs.premium
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('premium rate, 50k <= amount < 100k', (done) => {
    const values = valuesRange[2]
    const {name, multiplier} = programs.premium
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('premium rate, 100k <= amount', (done) => {
    const values = valuesRange[3]
    const {name, multiplier} = programs.premium
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('diamond rate, amount < 10k', (done) => {
    const values = valuesRange[0]
    const {name, multiplier} = programs.diamond
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier)
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier)

    done()
  })

  test('diamond rate, 10k <= amount < 50k', (done) => {
    const values = valuesRange[1]
    const {name, multiplier} = programs.diamond
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('diamond rate, 50k <= amount < 100k', (done) => {
    const values = valuesRange[2]
    const {name, multiplier} = programs.diamond
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('diamond rate, 100k <= amount', (done) => {
    const values = valuesRange[3]
    const {name, multiplier} = programs.diamond
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('random rate, amount < 10k', (done) => {
    const values = valuesRange[0]
    const {name, multiplier} = programs.random
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier)
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier)

    done()
  })

  test('random rate, 10k <= amount < 50k', (done) => {
    const values = valuesRange[1]
    const {name, multiplier} = programs.random
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('random rate, 50k <= amount < 100k', (done) => {
    const values = valuesRange[2]
    const {name, multiplier} = programs.random
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })

  test('random rate, 100k <= amount', (done) => {
    const values = valuesRange[3]
    const {name, multiplier} = programs.random
    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier)
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier)
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier)

    done()
  })
})
